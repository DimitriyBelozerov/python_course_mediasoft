def first():
    res = ""
    for i in range(99):
      if (i+1)%15 == 0:  
           res += "FizzBuzz" + "  "
      elif (i+1)%5 == 0:  
            res += "Buzz" + "  "
      elif (i+1)%3 == 0:  
            res += "Fizz" + "  "
      else:
           res += str(i) + "  "
    print(res)


def second():
    d = {
        'key1': 'value1',
        'key2': 'Value2'
        }
    g = {}
    
    for x, y in d.items():
        g[y] = x
    
    print(g)

def third():
    result = []
    list = [1, 1, 2, 3, 5, 4, 5, 5, 1, 6]
    for x in list:
       if x not in result:
         result.append(x)
    print(result)


def main():
    print("Введите номер выполняемого задания")
    print("1. Написать программу, которая выводит на экран числа от 1 до 100.  При этом вместо чисел, кратных трем, программа должна выводить слово «Fizz», а вместо чисел, кратных пяти — слово «Buzz». Если число кратно и 3, и 5, то программа должна выводить слово «FizzBuzz». \n")
    print("2. Написать программу, которая берет словарь и меняет местами ключи и значения. \n")
    print("3. Написать программу, которая берет исходный список и формирует новый список. В новом списке должны содержаться все элементы из исходного, за исключением дублей. \n")
    print("4. Выход \n")
    while True:
        answer = input()
        if answer == '1':
            first()
        elif answer == '2':
            second()
        elif answer == '3':
            third()
        elif answer == '4':
            break
        else:
            print("Введён неверный номер")

if __name__ == '__main__':
    main()

